/******************************************
           --- KLAMRISK HERO ---
Originally partycoded by lft and Yarrick
of kryo for Breakpoint 2010.
Updated to run on platform-agnostic SDL2
by CompuCat in 2019.
(Still compiles fine on SDL1/OpenGL too,
just use Makefile.sdl1 and you're all set!)

Notes: the SDL2 implementation is written
to actually render in 2D; this means quite
a few tricks achieved by rendering this 2D
game in 3D had to be changed around a bit.

For example, the he GL transformation
matrices are handled on CPU instead of on
GPU, and there is no handling of zdepth.
Yet.
******************************************/

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#ifdef SDL2
  #include <SDL2/SDL.h>
  #include <SDL2/SDL2_gfxPrimitives.h>
  #include <SDL2/SDL_ttf.h>
#else
  #include <SDL/SDL.h>
  #include <SDL/SDL_opengl.h>
  #include <SDL/SDL_ttf.h>
#endif
// These dimensions are in made-up units, where the width of an elevator shaft
// is 60 pixels, and pixels are square.

#define OUTERPOS 80
#define WALLPOS 30
#define SPLATTERPOS 20
#define DOORHEIGHT 95
#define FLOOR (0)		/* origo in middle of screen */

#define CIRCLEMAX 32

// Increase for higher resolution
#define CIRCLEDIM 128
#define NPARTICLE 256
#define SOUNDFREQ 44100

#define MAXDOOR 8

typedef enum {
	LEFT,
	RIGHT
} direction_t;

enum {
	T_CIRCLE,
	T_EXCLAM,
	T_KRYO,
	T_PRESENTS,
	T_KLAMRISK,
	T_HERO,
	T_LIVECODED,
	T_MENU,
	T_TRAINER,
	T_INSTRUCT1,
	T_INSTRUCT2,
	T_INSTRUCT3,
	T_1,
	T_2,
	T_3,
	T_4,
	NTEXTURE
};

struct particle {
	int			x, y;
	int			dx, dy;
	int			ddy;
	int			r;
};

struct shaft {
	int			alive;
	int			animframe;
	int			grace;
	direction_t		direction;
	struct particle		particle[NPARTICLE];
};

struct doors {
	int			ypos[MAXDOOR];		// coordinate of floor, disabled if <= -ymax
};

struct oscillator {
	uint16_t		freq, phase;
	int			volume;			// [0, 255]
};

int16_t synthesize();

#ifdef SDL2
SDL_Texture* texture[NTEXTURE];
#else
int texture[NTEXTURE]; //Textures used for circles and text
#endif
double texturesize[NTEXTURE][2];

// *************** Globals ***************

int screen_width, screen_height;	// in actual pixels
double ymax;				// in made-up units
#ifdef SDL2
  SDL_Window *screen;
  SDL_Renderer *renderer;
  SDL_Texture *zbuf[4];
#else
  SDL_Surface *screen;
#endif

int nbr_shafts;
int nbr_doors;
struct shaft shaft[10];
struct doors doors[10];

int appearance_timer, rate, playing, speed;

Uint32 menutime;
uint16_t freqtbl[64];
volatile struct oscillator osc[3];

TTF_Font *font;

#ifdef WIN32
extern char binary_Allerta_allerta_medium_ttf_start;
extern char binary_Allerta_allerta_medium_ttf_end;
#else
extern char _binary_Allerta_allerta_medium_ttf_start;
extern char _binary_Allerta_allerta_medium_ttf_end;
#endif

// *************** Setup ***************

static void cback(void *userdata, Uint8 *stream, int len) {
	int16_t *buf = (int16_t *) stream;
	int i, samples = len / sizeof(int16_t);

	for(i = 0; i < samples; i++) {
		buf[i] = synthesize();
	}
}

static int init_sdl() {
	int i;
	SDL_Rect **modes;
	SDL_AudioSpec spec = {
		.freq = SOUNDFREQ,
		.channels = 1,
		.format = AUDIO_S16,
		.samples = 512,
		.callback = cback
	};

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
		fprintf(stderr, "Unable to initialize SDL: %s\n", SDL_GetError());
		return 0;
	}
	atexit(SDL_Quit);

	#ifdef SDL2
    modes = 0; //HACKY //SDL_ListModes(0, SDL_FULLSCREEN | SDL_HWSURFACE | SDL_OPENGL);
  	if(modes == 0 || modes == (SDL_Rect **) -1) {
  		screen_width = 1024;
  		screen_height = 768;
  	} else {
  		screen_width = modes[0]->w;
  		screen_height = modes[0]->h;
  	}

    //Hack to convert from fullscreen to 640x480 for testing
    screen = SDL_CreateWindow("Klamrisk Hero", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screen_width, screen_height, SDL_WINDOW_RESIZABLE); //0, 0, SDL_WINDOW_FULLSCREEN_DESKTOP);
    renderer = SDL_CreateRenderer(screen, -1, 0);
    if (renderer==NULL){
      fprintf(stderr, "Unable to create renderer: %s\n", SDL_GetError());
      return 0;
    }

    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    //Initialize 4 textures to fake z-depth
    for(i=0; i<4; i++)
    {
      zbuf[i]=SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, screen_width, screen_height);
      SDL_SetTextureBlendMode(zbuf[i], SDL_BLENDMODE_BLEND);
      SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
      SDL_SetRenderTarget(renderer, zbuf[i]);
      SDL_RenderClear(renderer);
      SDL_SetRenderTarget(renderer, NULL);
    }

  #else
    modes = 0; //HACKY //SDL_ListModes(0, SDL_FULLSCREEN | SDL_HWSURFACE | SDL_OPENGL);
  	if(modes == 0 || modes == (SDL_Rect **) -1) {
  		screen_width = 640;
  		screen_height = 480;
  	} else {
  		screen_width = modes[0]->w;
  		screen_height = modes[0]->h;
  	}

  	screen = SDL_SetVideoMode(screen_width, screen_height, 0, SDL_HWSURFACE | SDL_OPENGL);// | SDL_FULLSCREEN);
    glGenTextures(NTEXTURE, texture);
  #endif
  if (!screen) {
    fprintf(stderr, "Unable to set video mode: %s\n", SDL_GetError());
    return 0;
  }

	if(SDL_OpenAudio(&spec, 0)) {
		fprintf(stderr, "Unable to set audio mode: %s\n", SDL_GetError());
		//return 0; //Hack to allow development on WSL, which has no audio devices. No big deal though.
	}

	ymax = 640.0 * screen_height / screen_width;

	for(i = 0; i < 64; i++) {
		freqtbl[i] = (uint16_t) round(440.0 * 65536.0 / SOUNDFREQ * pow(pow(2, 1.0/12), i - 39));
	}

	SDL_PauseAudio(0);
	SDL_ShowCursor(0);

	return 1;
}

static void maketext(int tid, char *text) {
	#ifdef SDL2
  SDL_Color color = {0, 0, 0};
  #else
  SDL_Color color = {255, 255, 255};
  #endif
	SDL_Surface *initial;
	double w, h;

	/* Use SDL_TTF to render our text */
	initial = TTF_RenderText_Blended(font, text, color);

	w = initial->w;
	h = initial->h;
	texturesize[tid][0] = w;
	texturesize[tid][1] = h;

  #ifdef SDL2
  texture[tid] = SDL_CreateTextureFromSurface(renderer, initial);
  #else
  /* Convert the rendered text to a known format */
	SDL_Surface *intermediary = SDL_CreateRGBSurface(0, w, h, 32,
			0x00ff0000, 0x0000ff00, 0x000000ff, 0x00000000);

	SDL_BlitSurface(initial, 0, intermediary, 0);

	/* Tell GL about our new texture */
	glBindTexture(GL_TEXTURE_2D, texture[tid]);
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, w, h, GL_RGBA, GL_UNSIGNED_BYTE, intermediary->pixels);

	/* GL_NEAREST looks horrible, if scaled... */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/* Bad things happen if we delete the texture before it finishes */
	//glFinish();

	/* Clean up */
	SDL_FreeSurface(intermediary);
  #endif
  SDL_FreeSurface(initial);
}

static void precalc() {
	#ifndef SDL2 //We draw circles directly in SDL2, no need for textures on quads
  int x, y;
	uint8_t circlebuf[CIRCLEDIM][CIRCLEDIM];
  for(y = 0; y < CIRCLEDIM; y++) {
		for(x = 0; x < CIRCLEDIM; x++) {
			int r = floor(sqrt(
				(y - CIRCLEDIM/2) * (y - CIRCLEDIM/2) +
				(x - CIRCLEDIM/2) * (x - CIRCLEDIM/2)));
			circlebuf[y][x] = (r < CIRCLEDIM/2)? 0xff : 0x00;
		}
	}
  glBindTexture(GL_TEXTURE_2D, texture[T_CIRCLE]);
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_ALPHA, CIRCLEDIM, CIRCLEDIM, GL_ALPHA, GL_UNSIGNED_BYTE, circlebuf);
  #endif

	maketext(T_EXCLAM, "!");
	maketext(T_KRYO, "kryo");
	maketext(T_PRESENTS, "presents");
	maketext(T_KLAMRISK, "KLAMRISK");
	maketext(T_HERO, "HERO");
	maketext(T_LIVECODED, "Live-coded at Breakpoint 2010");
	maketext(T_MENU, "Press space (or T for trainer)");
	maketext(T_TRAINER, "TRAINER MODE");
	maketext(T_INSTRUCT1, "Avoid dangerous edges.");
	maketext(T_INSTRUCT2, "Press 1 to toggle direction.");
	maketext(T_INSTRUCT3, "ESC to go back. Good luck!");
	maketext(T_1, "1");
	maketext(T_2, "2");
	maketext(T_3, "3");
	maketext(T_4, "4");
}

// *************** Sound ***************

int16_t synthesize() {
	int i;
	int accum = 0;

	osc[2].freq = freqtbl[(osc[2].volume >> 4) * 3];
	for(i = 0; i < 3; i++) {
		osc[i].phase += osc[i].freq;
		accum += ((osc[i].phase & 0x8000)? -1 : 1) * osc[i].volume;
	}

	return accum << 5;
}

void music() {
	int i, vol;
	static int timer = 0;
	static int octave = 0;
	static int currharm = 0;
	static int lastnote = 0;
	static int harmcount = 32;
	struct harmony {
		int		base;
		int		scale[8];
	} harmony[] = {
		{
			12,
			{24, 24, 26, 28, 29, 31, 36, 36}
		},
		{
			9,
			{21, 24, 26, 28, 31, 33, 35, 36}
		},
		{
			7,
			{23, 24, 26, 26, 28, 31, 35, 38}
		},
		{
			5,
			{21, 24, 26, 28, 29, 33, 36, 36}
		},
		{
			4,
			{23, 26, 28, 28, 31, 35, 35, 36}
		}
	};

	for(i = 0; i < 2; i++) {
		vol = osc[i].volume - 16;
		if(vol < 0) vol = 0;
		osc[i].volume = vol;
	}
	vol = osc[2].volume - 6;
	if(vol < 0) vol = 0;
	osc[2].volume = vol;

	if(timer) {
		timer--;
	} else {
		if(harmcount) {
			harmcount--;
		} else {
			currharm = rand() % (sizeof(harmony) / sizeof(*harmony));
			harmcount = ((rand() % 3) + 1) * 16;
		}
		osc[0].freq = freqtbl[harmony[currharm].base + (octave? 12 : 0)];
		osc[0].volume = 255;
		if(playing && (rand() % 4)) {
			lastnote += (rand() % 4) - 2;
			lastnote &= 7;
			osc[1].freq = freqtbl[harmony[currharm].scale[lastnote]];
			osc[1].volume = 255;
		}
		octave ^= 1;
		timer = 10;
	}
}

static void load_font() {
	TTF_Init();

#ifdef WIN32
	int len = (int) &binary_Allerta_allerta_medium_ttf_end;
	len -= (int) &binary_Allerta_allerta_medium_ttf_start;
	SDL_RWops *fontdata = SDL_RWFromMem(&binary_Allerta_allerta_medium_ttf_start, len);
#else
	int len = (int) &_binary_Allerta_allerta_medium_ttf_end;
	len -= (int) &_binary_Allerta_allerta_medium_ttf_start;
	SDL_RWops *fontdata = SDL_RWFromMem(&_binary_Allerta_allerta_medium_ttf_start, len);
#endif

	font = TTF_OpenFontRW(fontdata, 1, 100);
}

static int nextpoweroftwo(int x) { //Is this ever used anywhere? I don't think so...
	double logbase2 = log(x) / log(2);
	return round(pow(2,ceil(logbase2)));
}

// *************** Graphic primitives ***************

//Klamrisk Hero was originally written for raw OpenGL, which uses Cartesian coordinates.
//Matrix transformations were also used....liberally.
//These wrapper functions emulate the GL matrix stack CPU-side when needed, as 2D SDL2 has no such thing.
//Additionally, they translate given Cartesian coordinates to backend's coordinate system.
#ifdef SDL2

//Note the 3D instead of 4D matrix: we're deliberately ignoring zdepth here.
double matrixStack[5][3][3]={
  {{1,0,0},{0,1,0},{0,0,1}}, //Identity matrix times 5
  {{1,0,0},{0,1,0},{0,0,1}},
  {{1,0,0},{0,1,0},{0,0,1}},
  {{1,0,0},{0,1,0},{0,0,1}},
  {{1,0,0},{0,1,0},{0,0,1}},
};

int stackdeep=0;

static double xCartToSDL(double x) {return x+screen_width/2;}
static double yCartToSDL(double y) {return screen_height/2+y;}
static double xConvSDL(double x, double y) {return (x*matrixStack[0][0][0]+y*matrixStack[0][0][1]+matrixStack[0][0][2])+screen_width/2;}
static double yConvSDL(double x, double y) {return (x*matrixStack[0][1][0]+y*matrixStack[0][1][1]+matrixStack[0][1][2])+screen_height/2;}

#endif

//These behave like GL matrix functions:
//push() makes a duplicate of top matrix, and scale/rotate/translate() affect the top matrix (rather than replacing).
//These are *not* sanitychecked!
void resetMatrixStack(){
  int i, j, k;
  for(i=0; i<5; i++) for(j=0; j<3; j++) for(k=0; k<3; k++)
  {
    if(j==k) matrixStack[i][j][k]=1;
    else matrixStack[i][j][k]=0;
  }
}

void pushMatrix(){
  stackdeep++;
  #ifdef SDL2
  int i, j, k;
  for(i=1; i<5; i++) for(j=0; j<3; j++) for(k=0; k<3; k++) matrixStack[i][j][k]=matrixStack[i-1][j][k];
  //printf("Pushing %d %d %d %d %d %d %d %d %d\n", matrixStack[0][0][0], matrixStack[0][0][1], matrixStack[0][0][2], matrixStack[0][1][0], matrixStack[0][1][1], matrixStack[0][1][2], matrixStack[0][2][0], matrixStack[0][2][1], matrixStack[0][2][2]);
  #else
  glPushMatrix();
  #endif
}
void popMatrix(){
  stackdeep--;
  #ifdef SDL2
  int i, j, k;
  //Note, this leaves crap at the bottom of the stack, overpopping will give you crap.
  //printf("Popping %d %d %d %d %d %d %d %d %d, stack is now ", matrixStack[0][0][0], matrixStack[0][0][1], matrixStack[0][0][2], matrixStack[0][1][0], matrixStack[0][1][1], matrixStack[0][1][2], matrixStack[0][2][0], matrixStack[0][2][1], matrixStack[0][2][2]);
  for(i=0; i<4; i++) for(j=0; j<3; j++) for(k=0; k<3; k++) matrixStack[i][j][k]=matrixStack[i+1][j][k];
  //printf("%d %d %d %d %d %d %d %d %d\n", matrixStack[0][0][0], matrixStack[0][0][1], matrixStack[0][0][2], matrixStack[0][1][0], matrixStack[0][1][1], matrixStack[0][1][2], matrixStack[0][2][0], matrixStack[0][2][1], matrixStack[0][2][2]);
  #else
  glPopMatrix();
  #endif
}
void scale(int x, int y, int z){
  #ifdef SDL2
  int i;
  printf("x=%d, y=%d, %g %g %g %g %g %g %g %g %g -> ", x, y, matrixStack[0][0][0], matrixStack[0][0][1], matrixStack[0][0][2], matrixStack[0][1][0], matrixStack[0][1][1], matrixStack[0][1][2], matrixStack[0][2][0], matrixStack[0][2][1], matrixStack[0][2][2]);
  for(i=0; i<3; i++) {matrixStack[0][0][i]*=x; matrixStack[0][1][i]*=y;}
  printf("%g %g %g %g %g %g %g %g %g\n", matrixStack[0][0][0], matrixStack[0][0][1], matrixStack[0][0][2], matrixStack[0][1][0], matrixStack[0][1][1], matrixStack[0][1][2], matrixStack[0][2][0], matrixStack[0][2][1], matrixStack[0][2][2]);
  #else
  glScaled((double) x, (double) y, (double) z);
  #endif
}
void rotate(int angle, int x, int y, int z){
  #ifdef SDL2
  double a=angle*(M_PI/180);
  int i,j;
  double nm[3][3];
  for(i=0; i<3; i++)
  {
    nm[i][0]=cos(a)*matrixStack[0][i][0]+sin(a)*matrixStack[0][i][1];
    nm[i][1]=-sin(a)*matrixStack[0][i][0]+cos(a)*matrixStack[0][i][1];
  }
  for(i=0; i<3; i++) for(j=0; j<2; j++) matrixStack[0][i][j]=nm[i][j];
  #else
  glRotated((double) angle, (double) x, (double) y, (double) z);
  #endif
}
void translate(int x, int y, int z){
  #ifdef SDL2
  int i;
  for(i=0; i<3; i++) matrixStack[0][i][2]+=(x*matrixStack[0][i][0]+y*matrixStack[0][i][1]);
  #else
  glTranslated((double) x, (double) y, (double) z);
  #endif
}

static void fillrect(double x1, double y1, double x2, double y2, int z) {
	#ifdef SDL2
  SDL_Rect rect;
  rect.x=(int)xConvSDL(x1, y1);
  rect.y=(int)yConvSDL(x1, y1);
  rect.w=(int)(xConvSDL(x2, y2)-xConvSDL(x1, y1));
  rect.h=(int)(yConvSDL(x2, y2)-yConvSDL(x1, y1));
  //SDL_GetRenderDrawColor(renderer, &rect.r, &rect.g, &rect.b, &rect.a);
  //SDL_FillRect(zbuf[z], &rect, SDL_MapRGBA(zbuf[z]->format, r, g, b, a));
  //SDL_SetRenderTarget(renderer, zbuf[z]);
  SDL_RenderFillRect(renderer, &rect);
  //SDL_SetRenderTarget(renderer, NULL);

  #else
  glDisable(GL_TEXTURE_2D);
	glBegin(GL_QUADS);
	glVertex3d(x1, y1, z);
	glVertex3d(x1, y2, z);
	glVertex3d(x2, y2, z);
	glVertex3d(x2, y1, z);
	glEnd();
  #endif
}

static void draw_circle(double xpos, double ypos, double wspan, double hspan, double z) { //span is half width/height, pos is center
  #ifdef SDL2
  Uint8 r, g, b, a;
  SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);
  filledEllipseRGBA(renderer, (Sint16)xConvSDL(xpos, ypos), (Sint16) yConvSDL(xpos, ypos), (Sint16) wspan, (Sint16) hspan, r, g, b, a);
  //TODO: handle z coordinates?
  #else //Draws a circle using a...circle texture apparently.
  glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture[T_CIRCLE]);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //what exactly does this achieve?
	glBegin(GL_QUADS);
	glTexCoord2d(0, 0);
	glVertex3d(xpos - wspan, ypos - hspan, z);
	glTexCoord2d(0, 1);
	glVertex3d(xpos - wspan, ypos + hspan, z);
	glTexCoord2d(1, 1);
	glVertex3d(xpos + wspan, ypos + hspan, z);
	glTexCoord2d(1, 0);
	glVertex3d(xpos + wspan, ypos - hspan, z);
	glEnd();
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
  #endif
}

static void draw_triangle(double x1, double y1, double x2, double y2, double x3, double y3, double z) {
  #ifdef SDL2
  Uint8 r, g, b, a;
  SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);
  filledTrigonRGBA(renderer, (Sint16) xConvSDL(x1, y1), (Sint16) yConvSDL(x1, y1), (Sint16) xConvSDL(x2, y2), (Sint16) yConvSDL(x2, y2), (Sint16) xConvSDL(x3, y3), (Sint16) yConvSDL(x3, y3), r, g, b, a);
  #else
  glBegin(GL_TRIANGLES);
	glVertex3d(x1, y1, z);
	glVertex3d(x2, y2, z);
	glVertex3d(x3, y3, z);
	glEnd();
  #endif
}

static void set_current_color(double red, double green, double blue) {
  #ifdef SDL2
    SDL_SetRenderDrawColor(renderer, (Uint8)(red*255), (Uint8)(green*255), (Uint8)(blue*255), 255);
  #else
    glColor3d((GLdouble)red, (GLdouble)green, (GLdouble)blue);
  #endif
}

static void render_text(int tid, double x, double y, double zoom) {
  #ifdef SDL2
  SDL_Rect rect;
  rect.w=(int)(texturesize[tid][0]*zoom);
  rect.h=(int)(texturesize[tid][1]*zoom);
  rect.x=(int)xCartToSDL(x-rect.w/2);
  rect.y=(int)yCartToSDL(y);
  //Accuracy would dictate that we set blending to one-minus-source-color mode here.
  //Since SDL2 doesn't support that, we hardcode text to black in make_text() instead.
  SDL_RenderCopy(renderer, texture[tid], NULL, &rect);
  #else
  double w = texturesize[tid][0], h = texturesize[tid][1];

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture[tid]);
	glColor3f(1.0f, 1.0f, 1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE_MINUS_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR);

	/* Draw a quad at location */
	glBegin(GL_QUADS);
	glTexCoord2d(0, 0);
	glVertex2f(x - w/2 * zoom, y);
	glTexCoord2d(1, 0);
	glVertex2f(x + w/2 * zoom, y);
	glTexCoord2d(1, 1);
	glVertex2f(x + w/2 * zoom, y + h * zoom);
	glTexCoord2d(0, 1);
	glVertex2f(x - w/2 * zoom, y + h * zoom);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
  #endif
}

// *************** Complex drawing operations ***************

static void draw_doors(struct doors *d) {
	int i;

	// Draw doors on the right side. We're invoked under a flipped transformation in order to draw left doors.

	set_current_color(0, 0, 0);
	fillrect(WALLPOS+0, -ymax, WALLPOS+2, ymax, 3);
	for(i = 0; i < MAXDOOR; i++) {
		if(d->ypos[i] > -ymax) {
			set_current_color(1, 1, 1);
			fillrect(WALLPOS, d->ypos[i] - DOORHEIGHT, OUTERPOS, d->ypos[i], 1);
			fillrect(WALLPOS+4, d->ypos[i] - DOORHEIGHT, OUTERPOS, d->ypos[i], 3);
			set_current_color(0, 0, 0);
			fillrect(WALLPOS, d->ypos[i] - DOORHEIGHT, OUTERPOS, d->ypos[i] - DOORHEIGHT + 2, 3);
			fillrect(WALLPOS, d->ypos[i] - 4, OUTERPOS, d->ypos[i], 3);
			fillrect(WALLPOS+4, d->ypos[i] - DOORHEIGHT, WALLPOS+6, d->ypos[i], 3);
			fillrect(WALLPOS+8, d->ypos[i] - DOORHEIGHT, WALLPOS+10, d->ypos[i], 3);
		}
	}
}

static void draw_shaft(struct shaft *shaft, int tid, struct doors *left, struct doors *right, int xpos) {
	int i, angle, offset;
	double fade = 0;

	if(shaft->animframe > 10) {
		offset = -speed * (shaft->animframe - 10);
		if(shaft->animframe > 60) {
			fade = (shaft->animframe - 60.0) / 20.0;
			if(fade > 1) fade = 1;
		}
	} else {
		offset = shaft->grace;
	}
	pushMatrix();
		translate(xpos, 0, 0);

		set_current_color(0.83, 0.83, 0.83);
		fillrect(-OUTERPOS, -ymax, OUTERPOS, ymax, 50);
		set_current_color(1, 1, 1);
		fillrect(-WALLPOS-2, -ymax, WALLPOS+2, ymax, 30);

		// Draw the lift
		pushMatrix();
			translate(0, offset, 0);
			set_current_color(fade, fade, fade);
			fillrect(-WALLPOS, FLOOR, WALLPOS, FLOOR + 2, 30);
			fillrect(-WALLPOS, FLOOR - DOORHEIGHT, WALLPOS, FLOOR - DOORHEIGHT + 2, 30);
		popMatrix();

		// Draw doors on both sides
		draw_doors(right);
		pushMatrix();
			scale(-1, 1, 1);
			draw_doors(left);
		popMatrix();

		// Draw the contents of the lift
		pushMatrix();
			if(shaft->direction == LEFT) scale(-1, 1, 1);
			translate(0, offset, 0);
			pushMatrix();
				// Trash can
				double y = FLOOR - 2;
				y -= shaft->animframe * 0.8;
				if (y < FLOOR-DOORHEIGHT+63) y = FLOOR-DOORHEIGHT + 63;
				translate(28, y, 0);
				angle = shaft->animframe * 2;
				if(angle > 33) angle = 33;
				translate(-angle / 16, -angle / 2, 0);
				rotate(-angle, 0, 0, 1);
				set_current_color(fade, fade, fade);
				fillrect(-30, -52, 0, 0, 30);
				fillrect(-34, -52, 0, -55, 30);
				set_current_color(1, 1, 1);
				fillrect(-28, -50, -2, -2, 30);
				fillrect(-33, -53, -2, -54, 30);
				set_current_color(fade, fade, fade);
				draw_circle(-29, FLOOR-2, 4, 4, 30);
			popMatrix();
			pushMatrix();
				// Victim head
				y = shaft->animframe * -0.8 + 5;
				if (y < -27) y = -27;
				if (y > 0) y = 0;
				double skew = shaft->animframe * 0.6 - 15;
				if (skew < 0) skew = 0;
				if (skew > 2.6) skew = 2.6;
				translate(0, y, 0);
				set_current_color(fade, fade, fade);
				draw_circle(-19, -62, 9 + skew, 9 - skew, 30);
				// Victim body
				translate(-3 * skew, skew, 0);
				double fall = shaft->animframe  - 60;
				if (fall < 0) fall = 0;
				if (fall > 9) fall = 9;
				translate(skew, 0.3 * fall * fall, 0);
				draw_circle(-19, -32, 9 - skew, 20 + skew, 30);
				fillrect(-16, -15, -23, 0, 30);
			popMatrix();

		popMatrix();

		// Draw the red lemonade
		#ifndef SDL2
    glDepthFunc(GL_LESS);
    #endif
		set_current_color(1, 0, 0);
		if(shaft->direction == RIGHT) scale(-1, 1, 1);
		for(i = 0; i < NPARTICLE; i++) {
			if(shaft->particle[i].r > 0 && shaft->particle[i].y < (ymax + CIRCLEMAX) * 8) {
				draw_circle(shaft->particle[i].x / 8, shaft->particle[i].y / 8,
					shaft->particle[i].r, shaft->particle[i].r, 40 + i*.01);
			}
		}
    #ifndef SDL2
		glDepthFunc(GL_ALWAYS);
    #endif

	popMatrix();
	render_text(tid, xpos, -ymax + 40, .4);
}

static void drawtitle() {
	set_current_color(0, 0, 0);
  draw_triangle(0, -390, 400, 210, -400, 210, 0);
	set_current_color(.992, 1, .196);
  draw_triangle(0, -360, 370, 195, -370, 195, 0);
	render_text(T_EXCLAM, 0, -320, 1.5);
	render_text(T_KRYO, 0, -180, .5);
	render_text(T_PRESENTS, 0, -120, .4);
	render_text(T_KLAMRISK, 0, -25, .8);
	render_text(T_HERO, 0, 70, 1.3);
	render_text(T_LIVECODED, 0, 240, .4);
	render_text(T_MENU, 0, 290, .4);
	set_current_color(1, 0, 0);
	draw_circle(-78, -35, 6, 6, 0);
	draw_circle(-62, -35, 6, 6, 0);
}

static void drawframe() {
	int i;

  #ifdef SDL2
  set_current_color(.992, 1, .196);
  SDL_RenderClear(renderer);
  resetMatrixStack();

  #else
	// Set background
	glClearColor(.992, 1, .196, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION); //Load an orthogonal projection matrix, assuming this is setting the viewport
	glLoadIdentity();
	glOrtho(-640, 640, ymax, -ymax, -100, 100);
	glMatrixMode(GL_MODELVIEW); //Reset model view to the identity
	glLoadIdentity();
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_ALWAYS);
  #endif

	if(!playing) {
		drawtitle();
	} else {
		//todo
		//set_current_color(.47, .47, .47);
		//fillrect(-82, 0, 82, 480);

		for(i = 0; i < nbr_shafts; i++) {
			draw_shaft(&shaft[i], T_1 + i, &doors[i], &doors[i + 1], (160 * (i - 2) + 80) * (nbr_shafts > 1));
		}
		if(nbr_shafts == 1) {
			render_text(T_TRAINER, -600 + texturesize[T_TRAINER][0]/2 * .6, -290, .6);
			render_text(T_INSTRUCT1, -600 + texturesize[T_INSTRUCT1][0]/2 * .3, -200, .3);
			render_text(T_INSTRUCT2, -600 + texturesize[T_INSTRUCT2][0]/2 * .3, -150, .3);
			render_text(T_INSTRUCT3, -600 + texturesize[T_INSTRUCT3][0]/2 * .3, -100, .3);
		}
	}
  #ifdef SDL2
  /*for (i=0; i<4; i++) //Copy zbuffer layers to screen and clear them.
  {
    SDL_RenderCopy(renderer, zbuf[i], NULL, NULL);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_SetRenderTarget(renderer, zbuf[i]);
    SDL_RenderClear(renderer);
    SDL_SetRenderTarget(renderer, NULL);
  }*/
  #endif
}

// *************** Gameplay ***************

static void splat(struct shaft *shaft) {
	int i;

	for(i = 0; i < NPARTICLE; i++) {
		shaft->particle[i].x = SPLATTERPOS * 8;
		shaft->particle[i].y = (FLOOR - 63) * 8;
		shaft->particle[i].dx = (rand() % 64) - 32;
		shaft->particle[i].dy = (rand() % 64) - 48;
		shaft->particle[i].r = rand() % (CIRCLEMAX / 2);
		shaft->particle[i].ddy = 2;
	}
}

static void doors_physics(struct doors *d) {
	int i;

	for(i = 0; i < MAXDOOR; i++) {
		if(d->ypos[i] > -ymax) {
			d->ypos[i] -= speed;
		}
	}
}

static void shaft_physics(struct shaft *shaft, struct doors *left, struct doors *right) {
	int i;

	if(shaft->alive) {
		struct doors *dangerous = (shaft->direction == LEFT)? left : right;

		if(!shaft->grace) {
			for(i = 0; i < MAXDOOR; i++) {
				if(dangerous->ypos[i] > FLOOR - 3 && dangerous->ypos[i] < FLOOR) {
					shaft->alive = 0;
				}
			}
		}
		shaft->grace -= speed;
		if(shaft->grace < 0) shaft->grace = 0;
	} else {
		shaft->animframe++;
		if(shaft->animframe == 15) {
			splat(shaft);
			osc[2].volume = 255;
		} else if(shaft->animframe == 45) {
			//speed++;
		} else if(shaft->animframe == 100) {
			shaft->alive = 1;
			shaft->animframe = 0;
			shaft->grace = ymax + DOORHEIGHT;
		}
	}
	for(i = 0; i < NPARTICLE; i++) {
		if(shaft->particle[i].r > 0 && shaft->particle[i].y < (ymax + CIRCLEMAX) * 8) {
			shaft->particle[i].x += shaft->particle[i].dx;
			if(shaft->particle[i].x > WALLPOS * 8) {
				shaft->particle[i].x -= shaft->particle[i].dx;
				shaft->particle[i].dx *= -.1;
				if(rand() % 3) {
					shaft->particle[i].ddy = 0;
					if(shaft->particle[i].dy < 0) shaft->particle[i].dy = 0;
					shaft->particle[i].dx = 0;
				}
			}
			if(shaft->particle[i].x < -WALLPOS * 8) {
				shaft->particle[i].x -= shaft->particle[i].dx;
				shaft->particle[i].dx *= -.1;
				if(rand() % 3) {
					shaft->particle[i].ddy = 0;
					if(shaft->particle[i].dy < 0) shaft->particle[i].dy = 0;
					shaft->particle[i].dx = 0;
				}
			}
			shaft->particle[i].y += shaft->particle[i].dy;
			shaft->particle[i].dy += shaft->particle[i].ddy;
			if(shaft->particle[i].r && !(rand() % 5)) shaft->particle[i].r--;
		}
	}
}

static void resetshaft(struct shaft *shaft) {
	memset(shaft, 0, sizeof(struct shaft));
	shaft->alive = 1;
	shaft->direction = LEFT;
}

static void resetdoors(struct doors *d) {
	int i;

	for(i = 0; i < MAXDOOR; i++) {
		d->ypos[i] = -ymax;
	}
}

static void flip(struct shaft *shaft) {
	if(shaft->alive) {
		shaft->direction ^= (LEFT ^ RIGHT);
	}
}

static void newgame(int shafts) {
	int i;

	if (shafts > 4)
		return;

	playing = 1;
	nbr_doors = shafts+1;
	nbr_shafts = shafts;

	for(i = 0; i < shafts; i++) {
		resetshaft(&shaft[i]);
	}
	for(i = 0; i < shafts + 1; i++) {
		resetdoors(&doors[i]);
	}
	rate = 50;
	appearance_timer = rate;
	speed = 2;
}

static void add_door() {
	int which = rand() % nbr_doors;
	int i;

	for(i = 0; i < MAXDOOR; i++) {
		if(doors[which].ypos[i] <= -ymax) {
			doors[which].ypos[i] = ymax + DOORHEIGHT;
			break;
		}
	}
}

static void add_doors() {
	if(rate) {
		if(appearance_timer) {
			appearance_timer--;
		} else {
			add_door();
			appearance_timer = rate;
		}
	}
}

int main(int argc, char *argv[])
{
	int i;
	int running = 1;
	Uint32 lasttick;

	init_sdl();
	load_font();
	if (!font) return 1;
	precalc();

	srand(time(0));

	lasttick = SDL_GetTicks();
	playing = 0;
	menutime = lasttick;
	while (running) {
		SDL_Event event;
		Uint32 now = SDL_GetTicks();
		while(now - lasttick > 20) {
			lasttick += 20;
			if(playing) {
				for(i = 0; i < nbr_doors; i++) {
					doors_physics(&doors[i]);
				}
				for(i = 0; i < nbr_shafts; i++) {
					shaft_physics(&shaft[i], &doors[i], &doors[i + 1]);
				}
			}
			music();
			add_doors();
		}

		while ( SDL_PollEvent(&event) ) {
			switch (event.type) {
				case SDL_QUIT:
					running = 0;
					break;
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym) {
						case SDLK_1:
							flip(&shaft[0]);
							break;
						case SDLK_2:
							flip(&shaft[1]);
							break;
						case SDLK_3:
							flip(&shaft[2]);
							break;
						case SDLK_4:
							flip(&shaft[3]);
							break;
						case SDLK_SPACE:
							if (!playing)
								newgame(4);
							break;
						case SDLK_t:
							if (!playing)
								newgame(1);
							break;
						case SDLK_ESCAPE:
							if (playing) {
								playing = 0;
								menutime = lasttick;
							} else {
								running = 0;
							}
							break;
					}
					break;
			}
		}

		drawframe();

		// Flip
		#ifdef SDL2
      SDL_RenderPresent(renderer);
      //SDL_GL_SwapWindow(screen);
      //SDL_UpdateWindowSurface(screen); //TODO: need both?
    #else
      SDL_GL_SwapBuffers();
    #endif
	}
	return 0;
}
