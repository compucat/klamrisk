CC=gcc

all: klamrisk

both: klamrisk klamrisk.exe

font.o: Allerta/allerta_medium.ttf
	objcopy --input binary --output elf64-x86-64 --binary-architecture i386:x86-64 $^ $@

fontwin.o: Allerta/allerta_medium.ttf
	i686-w64-mingw32-objcopy --input binary --output pe-i386 --binary-architecture i386 $^ $@

klamrisk.exe: klamrisk.c fontwin.o
	i686-w64-mingw32-gcc $^ -ISDL2-2.0.10/i686-w64-mingw32/include -LSDL2-2.0.10/i686-w64-mingw32/lib -lmingw32 -lSDL2main -lSDL2 -lSDL2_gfx -lopengl32 -lglu32 -lSDL2_ttf -o $@ -DWIN32 -DSDL2
	i686-w64-mingw32-strip $@

klamrisk: klamrisk.c font.o
	gcc -g -std=c99 -o $@ $^ -lm -lSDL2 -lGL -lGLU -lSDL2_ttf -lSDL2_gfx -DSDL2
	strip $@

clean:
	rm -f klamrisk klamrisk.exe font.o fontwin.o
